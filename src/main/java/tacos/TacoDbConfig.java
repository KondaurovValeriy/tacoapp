package tacos;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

//@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "tacoEntityManagerFactory",
        transactionManagerRef = "tacoTransactionManager",
        basePackages = {"tacos.repo.taco"})
public class TacoDbConfig {

    @Bean
    @ConfigurationProperties("app.datasource.first")
    public DataSourceProperties tacoDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean(name = "tacoDataSource")
    public DataSource tacoDataSource() {
        return tacoDataSourceProperties().initializeDataSourceBuilder().build();
    }

    @Bean(name = "tacoEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean tacoEntityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("tacoDataSource") DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .packages("tacos.domain.taco")
                .persistenceUnit("taco")
                .build();
    }

    @Bean(name = "tacoTransactionManager")
    public PlatformTransactionManager tacoTransactionManager(
            @Qualifier("tacoEntityManagerFactory") EntityManagerFactory tacoEntityManagerFactory) {
        return new JpaTransactionManager(tacoEntityManagerFactory);
    }

}
