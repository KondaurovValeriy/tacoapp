package tacos.data;


import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

import org.springframework.context.annotation.*;

import javax.sql.DataSource;

@Slf4j
@Configuration
public class RepoConfig{
    @Primary
    @Bean
    @ConfigurationProperties("app.datasource.first")
    public DataSourceProperties firstDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Primary
    @Bean(name = "h2")
    public DataSource dataSourceH2() {
        return firstDataSourceProperties().initializeDataSourceBuilder().build();
//        EmbeddedDatabaseBuilder dbBinder  = new EmbeddedDatabaseBuilder();
//        return dbBinder.setType(EmbeddedDatabaseType.H2).build();
    }

    @Bean
    @ConfigurationProperties("app.datasource.second")
    public DataSourceProperties secondDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean(name = "pg")
    @ConfigurationProperties("app.datasource.second.configuration")
    public DataSource dataSourcePg() {
        return secondDataSourceProperties().initializeDataSourceBuilder().build();
    }


}
